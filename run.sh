#!/usr/bin/env bash

source ./venv/bin/activate

OUTPUT_FILE=$1

pip install -e .
python ./cashify/main.py ${OUTPUT_FILE}
