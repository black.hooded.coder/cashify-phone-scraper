#!/usr/bin/env bash

python3 -m venv ./venv
source ./venv/bin/activate

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

rm get-pip.py

pip install --upgrade pip
pip install -r requirements.txt
pip install -e .
#python setup.py install
