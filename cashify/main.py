import os
import sys
from inspect import currentframe, getframeinfo
from pathlib import Path

from cashify.writer import db_to_csv

THIS_PATH = Path(getframeinfo(currentframe()).filename).resolve().parent
filename = sys.argv[1]
extension = '.csv'
# filename = 'output'

if not filename.endswith(extension):
    filename = f'{filename}{extension}'
OUTPUT = os.path.join(THIS_PATH, '..', 'output', filename)

try:
    os.remove(OUTPUT)
except FileNotFoundError:
    pass


def run_crawler():
    print('Crawling...this can take a few minutes...')
    # cmdline.execute(f'scrapy crawl phones -o {OUTPUT} --loglevel ERROR'.split())
    os.system(f'scrapy crawl phones --loglevel INFO')
    print('Done crawling!')
    print('Writing to CSV file...')
    db_to_csv(os.path.join(THIS_PATH, '..', 'output', filename))
    print('All done!')


if __name__ == '__main__':
    run_crawler()
