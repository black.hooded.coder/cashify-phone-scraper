# -*- coding: utf-8 -*-
import datetime
import os
import re

import scrapy

THIS_PATH = os.path.dirname(os.path.realpath(__file__))

BASE_URL = 'https://www.cashify.in'
GET_UPTO = "//div[@class='flwidth web-view']//meta[2]/@content"
SPECS_BOX = "//div[@class='flex-2 flheight flex-column pd-info']//" \
            "div[@class='flex-row align-center specs-box flex-wrap ng-star-inserted']"
SPECS_TOP = "//div[@class='flex-2 flheight flex-column pd-info']//span[@class='font-caption-1 tc-primary']/text()"
SPECS_BOTTOM = "//div[@class='flex-2 flheight flex-column pd-info']//span[@class='font-body-2 tc-secondary']/text()"

MODEL = "/html[1]/body[1]/csh-root[1]/div[1]/main[1]/csh-search-dashboard[1]/section[1]/div[1]/div[1]/div[1]/" \
        "csh-breadcrumb[1]/div[1]/div[4]/a[1]/text()"

PARENS = re.compile(r'\([^)]+\)')


class PhonesSpider(scrapy.Spider):
    name = 'phones'
    allowed_domains = ['cashify.in']
    start_urls = open(os.path.join(THIS_PATH, '..', 'pages_to_crawl.txt')).readlines()
    date = datetime.datetime.now()  # + datetime.timedelta(days=1)
    current_date = date.strftime('%Y-%m-%d')
    print('CURRENT_DATE', current_date)

    def parse_brand_models(self, response):
        # /sell-old-mobile-phone/used-apple-
        model_links = response.xpath(
            '//a[contains(@href, "/sell-old-mobile-phone/used-")]/@href').getall()

        for model in model_links:
            yield scrapy.Request(url=f'{BASE_URL}{model}', callback=self.parse_brand_variants)

    def parse_brand_variants(self, response):
        max_price = response.xpath(GET_UPTO).get()
        # https://www.cashify.in/sell-old-mobile-phone/used-apple-iphone-x-3-gb-64-gb
        if not max_price:
            variants = response.xpath('//a[contains(@href, "/sell-old-mobile-phone/used-")]/@href').getall()
            for var in variants:
                yield scrapy.Request(url=f'{BASE_URL}{var}', callback=self.parse_brand_variants)
        else:
            brand = response.url.split('/')[-1].split('-')[1]
            specs_top = response.xpath(SPECS_TOP).getall()
            specs_bottom = response.xpath(SPECS_BOTTOM).getall()
            specs = {bottom: top for top, bottom in zip(specs_top, specs_bottom)}
            model = response.xpath(MODEL).get()
            # model = PARENS.sub('', model).strip()
            yield {
                'brand': brand.capitalize(),
                'model': model,
                'specs': specs,
                'upto': float(max_price),  # + 1,
                'url': response.url,
                'datetime': self.current_date,
            }

    def parse(self, response):
        # first level - list of brands
        brand_links = response.xpath(
            '//a[contains(@href, "/sell-old-mobile-phone/sell-")]/@href').getall()
        for link in brand_links:
            yield scrapy.Request(url=f'{BASE_URL}{link}', callback=self.parse_brand_models)
