import csv
import sqlite3

from cashify.settings import DB_PATH

conn = sqlite3.connect(DB_PATH)
cur = conn.cursor()

REMOVE_COLUMNS = ('id', 'date', 'price')


def get_distinct_dates():
    sql = 'SELECT DISTINCT date FROM phones;'
    [dates] = cur.execute(sql).fetchall()
    return sorted(dates)


def make_columns():
    cur.execute('SELECT * FROM phones')
    names = [description[0] for description in cur.description if description[0] not in REMOVE_COLUMNS]
    dates = get_distinct_dates()
    return names + dates


def get_rows():
    results = {}
    query = 'SELECT * FROM phones;'

    for row in cur.execute(query).fetchall():
        _, brand, model, cores, storage, ram, camera, chipset, price, url, date = row
        if url in results:
            results[url][date] = price
        else:
            results[url] = {
                'Brand': brand,
                'Model': model,
                'Cores': cores,
                'Storage': storage,
                'RAM': ram,
                'Back Camera': camera,
                'Chipset': chipset,
                date: price,
            }
    return results


def write_csv(data, output):
    print(f'Saving output to {output}')
    values = list(data.values())
    keys = values[0].keys()
    with open(output, 'w') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(values)


def db_to_csv(output):
    write_csv(get_rows(), output)
    conn.close()
