# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import sqlite3
from sqlite3 import IntegrityError

from cashify.settings import DB_PATH


class CashifyPipeline:

    def __init__(self):
        self._conn = None
        self._cur = None
        self.get_db()
        self.create_tables()

    def get_db(self):
        self._conn = sqlite3.connect(DB_PATH)
        self._cur = self._conn.cursor()

    def create_tables(self):
        self.create_table()

    def drop_table(self):
        # drop table if it exists
        self._cur.execute("DROP TABLE IF EXISTS phones")

    def close_db(self):
        self._conn.close()

    def __del__(self):
        self.close_db()

    def create_table(self):
        sql = """
            CREATE TABLE IF NOT EXISTS phones(
            id INTEGER PRIMARY KEY NOT NULL,
            brand TEXT,
            model TEXT,
            cores INTEGER,
            storage TEXT,
            ram TEXT,
            back_camera TEXT,
            chipset TEXT,
            price INTEGER,
            url TEXT,
            date TEXT,
            UNIQUE(model, price))
        """
        self._cur.execute(sql)

    def process_item(self, item, spider):
        self.insert(item)
        return item

    def insert(self, item):
        sql = """
            INSERT INTO phones(brand, model, cores, storage, ram, back_camera, chipset, price, url, date) 
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
        try:
            self._cur.execute(sql, (
                item.get('brand', ''),
                item.get('model', ''),
                item.get('specs').get('Cores', ''),
                item.get('specs').get('Storage', ''),
                item.get('specs').get('RAM', ''),
                item.get('specs').get('Back Camera', ''),
                item.get('specs').get('Chipset', ''),
                item.get('upto'),
                item.get('url'),
                item.get('datetime'),
            ))
        except IntegrityError:
            print('dupe')
            pass
        finally:
            self._conn.commit()
